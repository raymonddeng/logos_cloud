const functions = require('firebase-functions');
const admin = require('firebase-admin');

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hilogos-17e53.firebaseio.com"
});

const twilioAccountSid = 'ACe909fc7006454ad694d2efa2ace9a0fc';
const twilioAuthToken = '04a5864356251fd00be80737c0e7f124'
const billed_twilio_num = '+12053584539';

const client = require('twilio')(twilioAccountSid, twilioAuthToken);

exports.remindUsersCronJob = functions.pubsub.schedule("every day 10:00").onRun(async () => {
	/*
	remindUsers can be put in here, but Firebase bills for cron jobs:

	"Error: HTTP Error: 400, Billing must be enabled for 
	activation of service '[cloudscheduler.googleapis.com]' in project '925884863652' to proceed."
	*/
})


exports.remindUsers = functions.https.onCall(async (req, res) => {
	// This uses Firestore! I missed seeing 'Realtime DB' on the assignment sheet the first time I read it, 
	// and since this is something extra I'm just going to leave this as is when I submit my work.
	var users = admin.firestore().collection('users');


	// Get users collection
	return Promise.resolve(users.get()).then(querySnapshot => {

		// Iterate through users
		querySnapshot.forEach(async user => {

			// For each user, collect how many unfinished tasks they have
			const user_info = user.data();
			const user_phone_num = user_info.phone_num;
			const user_remaining_tasks = [];

			for (var i = 0; i < user_info.tasks.length; i++){
				if (user_info.tasks[i].completed == false){
					user_remaining_tasks.push(user_info.tasks[i].desc)
				}
			}

			// If they have remaining tasks and a registered number, send a text message using Twilio
			if (user_remaining_tasks.length > 0 && typeof(user_phone_num) !== 'undefined'){
				const msg = 'Good morning! Some tasks for today: ' + user_remaining_tasks.join(', ');
				await client.messages.create({body: msg, from: billed_twilio_num, to: user_phone_num}).then(res =>{
					console.log('Message sent!');
				});
			}
		})

		return {"result":200}
	});
})